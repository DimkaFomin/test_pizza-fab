<?php
/**
 * Created by PhpStorm.
 * User: Дмитрий
 * Date: 10.03.2019
 * Time: 13:33
 */


class Order
{
    public $number;
    public $arrival_time;
    public $cooking_time;
    public $fact_time_start;
    public $fact_time_end;
    public $destination_coords_x;
    public $destination_coords_y;

    function __construct($number, $arrival_time, $cooking_time,
                         $fact_time_end, $destination_coords_x, $destination_coords_y)
    {
        $this->number = $number;
        $this->arrival_time = $arrival_time;
        $this->cooking_time = $cooking_time;
        $this->fact_time_end = $fact_time_end;
        $this->destination_coords_x = $destination_coords_x;
        $this->destination_coords_y = $destination_coords_y;
    }


    function getNumber(){
        return $this->number;
    }
    function getArrivalTime(){
        return $this->arrival_time;
    }
    function getCookingTime(){
        return $this->cooking_time;
    }
    function getFactTimeEnd(){
        return $this->fact_time_end;
    }
    function getDestinationCoordsX(){
        return $this->destination_coords_x;
    }
    function getDestinationCoordsY(){
        return $this->destination_coords_y;
    }
}

class Route{
    public $order1_id;
    public $order1_destination_time;
    public $order2_id;
    public $order2_destination_time;
    public $order3_id;
    public $order3_destination_time;

    function __construct(array $order_ids, array $orders_destination_times)
    {


       if(count($order_ids) == 1){
           $this->order1_id = $order_ids[0];
           $this->order1_destination_time = $orders_destination_times[0];
       }
       elseif(func_num_args() == 2){
           $this->order1_id = $order_ids[0];
           $this->order1_destination_time =  $orders_destination_times[0];
           $this->order2_id = $order_ids[1];
           $this->order2_destination_time =  $orders_destination_times[1];
       }
       else{
           $this->order1_id = $order_ids[0];
           $this->order1_destination_time =  $orders_destination_times[0];
           $this->order2_id = $order_ids[1];
           $this->order2_destination_time =  $orders_destination_times[1];
           $this->order3_id = $order_ids[2];
           $this->order3_destination_time =  $orders_destination_times[2];
       }
    }

    public function getOrder1ID(){
        return $this->order1_id;
    }
    public function getOrder1DestinationTime(){
        return $this->order1_destination_time;
    }
    public function getOrder2ID(){
        return $this->order2_id;
    }
    public function getOrder2DestinationTime(){
        return $this->order2_destination_time;
    }
    public function getOrder3ID(){
        return $this->order3_id;
    }
    public function getOrder3DestinationTime(){
        return $this->order3_destination_time;
    }
}

class Order_list extends ArrayObject {}
class Routes_list extends ArrayObject {}


function distance_between_two_points($x2, $x1, $y2, $y1){
    $distance = sqrt(pow($x2-$x1, 2) + pow($y2-$y1, 2));
    return $distance;
}

function sort_by_ready_time($all_orders){
    for ($n = 0; $n < count($all_orders) - 1; $n++){
        if($all_orders[$n]->getFactTimeEnd() > $all_orders[$n+1]->getFactTimeEnd()){
            $buf = $all_orders[$n];
            $all_orders[$n] = $all_orders[$n+1];
            $all_orders[$n+1] = $buf;
            sort_by_ready_time($all_orders);
        }
    }
    return $all_orders;
}


function can_take_second(Order $or1, Order $or2){
    $max_time_from_call_to_arrival = 60;
    $speed = 60;

    $or1_x = $or1->getDestinationCoordsX();
    $or1_y = $or1->getDestinationCoordsY();
    $or2_x = $or2->getDestinationCoordsX();
    $or2_y = $or2->getDestinationCoordsY();
    $or1_fact_time_end = $or1->getFactTimeEnd();
    $or2_fact_time_end = $or2->getFactTimeEnd();
    $or1_arrival_time = $or1->getArrivalTime();
    $or2_arrival_time = $or2->getArrivalTime();
    $or1_number = $or1->getNumber();
    $or2_number = $or2->getNumber();


    $max_time_destination_or1 = $or1_arrival_time + $max_time_from_call_to_arrival;
    $time_from_center_to_first_order = distance_between_two_points($or1_x, 0 , $or1_y, 0)/$speed;

    $difference_in_time_first_and_second = $or2_fact_time_end - $or1_fact_time_end;
    $max_time_destination_or2 = $or2_arrival_time + $max_time_from_call_to_arrival;
    $time_from_or1_to_or2 = distance_between_two_points($or2_x, $or1_x, $or2_y,$or1_y) / $speed;

    $time_sum1 = $or1_fact_time_end + $time_from_center_to_first_order + $difference_in_time_first_and_second;
    $time_sum2 = $or1_fact_time_end + $time_from_center_to_first_order + $difference_in_time_first_and_second + $time_from_or1_to_or2;

    if($max_time_destination_or1 > $time_sum1 and $max_time_destination_or2 > $time_sum2){
        $return = array(true, $or1_number, $time_sum1,
            $or2_number, $time_sum2);
        return $return;
    }
    else{
        $return = array(false,$or1_number, $or1_fact_time_end + $time_from_center_to_first_order);
        return $return;
    }
}

function can_take_third(Order $or1, Order $or2, Order $or3){
    $max_time_from_call_to_arrival = 60;
    $speed = 60;

    $or1_x = $or1->getDestinationCoordsX();
    $or1_y = $or1->getDestinationCoordsY();
    $or2_x = $or2->getDestinationCoordsX();
    $or2_y = $or2->getDestinationCoordsY();
    $or3_x = $or3->getDestinationCoordsX();
    $or3_y = $or3->getDestinationCoordsY();
    $or1_arrival_time = $or1->getArrivalTime();
    $or2_arrival_time = $or2->getArrivalTime();
    $or3_arrival_time = $or3->getArrivalTime();
    $or3_fact_time_end = $or3->getFactTimeEnd();
    $or1_fact_time_end = $or1->getFactTimeEnd();
    $or2_fact_time_end = $or2->getFactTimeEnd();
    $or1_number = $or1->getNumber();
    $or2_number = $or2->getNumber();
    $or3_number = $or3->getNumber();

    $time_from_center_to_first_order = distance_between_two_points($or1_x, 0 , $or1_y, 0)/$speed;
    $max_destination_time_or1 = $or1_arrival_time + $max_time_from_call_to_arrival;

    $max_destination_time_or2 = $or2_arrival_time + $max_time_from_call_to_arrival;
    $time_from_first_to_second = distance_between_two_points($or2_x, $or1_x, $or2_y, $or1_y) / $speed;

    $difference_in_time_or1_or3 = $or3_fact_time_end - $or1_arrival_time;
    $max_destination_time_or3 = $or3_arrival_time + $max_time_from_call_to_arrival;
    $time_from_second_to_third = distance_between_two_points($or3_x, $or2_x, $or3_y, $or2_y) / $speed;

    $time_sum1 = $or1_fact_time_end + $difference_in_time_or1_or3 + $time_from_center_to_first_order;
    $time_sum2 = $or1_fact_time_end + $difference_in_time_or1_or3 + $time_from_center_to_first_order + $time_from_first_to_second;
    $time_sum3 = $or1_fact_time_end + $difference_in_time_or1_or3 + $time_from_center_to_first_order + $time_from_first_to_second + $time_from_second_to_third;

    if($max_destination_time_or1 > $time_sum1 && $max_destination_time_or2 > $time_sum2 && $max_destination_time_or3 > $time_sum3){
        $return = array(true, $or1_number, $time_sum1,
            $or2_number, $time_sum2,
            $or3_number, $time_sum3);
        return $return;
    }
    else{
        return array(false);
    }
}

function make_one_route(Order $or1, Order $or2, Order $or3){
    $check_second = can_take_second($or1, $or2);

    if($check_second[0] == true){
        $check_third = can_take_third($or1, $or2, $or3);
        if($check_third[0] == true){
            $order_ids = array($check_third[1], $check_third[3], $check_third[5]);
            $orders_destination_times = array($check_third[2], $check_third[4], $check_third[6]);
            $route = new Route($order_ids, $orders_destination_times);
            $step_to_iternal_loop = 3;
        }
        else{
            $order_ids = array($check_second[1], $check_second[3]);
            $orders_destination_times = array($check_second[2], $check_second[4]);
            $route = new Route($order_ids, $orders_destination_times);
            $step_to_iternal_loop = 2;
        }
    }
    else{
        $order_ids = array($check_second[1]);
        $orders_destination_times = array($check_second[2]);
        $route = new Route($order_ids, $orders_destination_times);
        $step_to_iternal_loop = 1;
    }

    $return = array($step_to_iternal_loop, $route);
    return $return;

}

/*определение констант*/

const min_orders_count = 10;
const max_orders_count = 100;
const interval_arrival_min = 1;
const interval_arrival_max = 30;
const interval_cooking_time_min = 10;
const interval_cooking_time_max = 30;
const min_coords_value = -1000;
const max_coords_value = 1000;

$N = rand(min_orders_count, max_orders_count);
$arrival_time=0;

$all_orders = new Order_list();
$all_routes = new Routes_list();

for ($n = 0; $n < $N; $n++){

    $arrival_time = $arrival_time + rand(interval_arrival_min, interval_arrival_max);
    $cooking_time = rand(interval_cooking_time_min, interval_cooking_time_max);

    $destination_coords_x = rand(min_coords_value, max_coords_value);
    $destination_coords_y = rand(min_coords_value, max_coords_value);


    if($n == 0 or $n == 1) {
        $fact_time_start = $arrival_time;
        $fact_time_end = $cooking_time + $arrival_time;
    }
    else{

        //var_dump($all_orders[$n]);
        //return;
        if($all_orders[$n-2]->getFactTimeEnd() > $all_orders[$n-1]->getFactTimeEnd()){
            if($arrival_time > $all_orders[$n-1]->getFactTimeEnd()){
                $fact_time_start = $arrival_time;
                $fact_time_end = $arrival_time + $cooking_time;
            }else{
                $fact_time_start = $all_orders[$n-1]->getFactTimeEnd();
                $fact_time_end = $all_orders[$n-1]->getFactTimeEnd() + $cooking_time;
            }
        }
        else{
            if($arrival_time > $all_orders[$n-2]->getFactTimeEnd()){
                $fact_time_start = $arrival_time;
                $fact_time_end = $arrival_time + $cooking_time;
            }else{
                $fact_time_start = $all_orders[$n-2]->getFactTimeEnd();
                $fact_time_end = $all_orders[$n-2]->getFactTimeEnd() + $cooking_time;
            }
        }
    }


    $order = new Order($n, $arrival_time, $cooking_time, $fact_time_end, $destination_coords_x,
        $destination_coords_y);

    $all_orders->append($order);
}

foreach($all_orders as $order){
    print_r('Номер заказа:'.$order->getNumber().' '.
        'Время поступления: '.$order->getArrivalTime().' '.
        'Время готовки: '.$order->getCookingTime().' '.
        'Координата X: '.$order->getDestinationCoordsX().' '.
        'Координата Y: '.$order->getDestinationCoordsY().' '.
        "\xA");
}

$sort_orders = sort_by_ready_time($all_orders);

$position_in_loop = 0;

//формирование маршрутов до заказа N-2
do{

    $or1 = $sort_orders[$position_in_loop];
    $or2 = $sort_orders[$position_in_loop + 1];
    $or3 = $sort_orders[$position_in_loop + 2];
    $res_array = make_one_route($or1, $or2, $or3);

    $route = $res_array[1];
    $step = $res_array[0];
    $all_routes->append($route);
    $position_in_loop = $position_in_loop + $step;
}
while($position_in_loop < $N-2);

//формирование маршрутов при заказе N-2 и следующий
if($position_in_loop != $N && $position_in_loop == $N-2){
    $check_second = can_take_second($sort_orders[$position_in_loop], $sort_orders[$position_in_loop + 1]);
	//если можем взять последние два заказа в один маршрут
    if($check_second[0]){
        $order_ids = array($check_second[1], $check_second[3]);
        $orders_destination_times = array($check_second[2], $check_second[4]);
        $route = new Route($order_ids, $orders_destination_times);
        $all_routes->append($route);
    }
	//не можем взять 2 последние заказа в один маршрут
    else{
        $max_time_from_call_to_arrival = 60;
        $speed = 60;
        $or1_number = $sort_orders[$position_in_loop]->getNumber();
        $or2_number = $sort_orders[$position_in_loop + 1]->getNumber();
        $or1_x = $sort_orders[$position_in_loop]->getDestinationCoordsX();
        $or1_y = $sort_orders[$position_in_loop]->getDestinationCoordsY();
        $or2_x = $sort_orders[$position_in_loop + 1]->getDestinationCoordsX();
        $or2_y = $sort_orders[$position_in_loop + 1]->getDestinationCoordsY();


        $time_from_center_to_or1 = distance_between_two_points($or1_x, 0 , $or1_y, 0) / $speed;
        $time_from_center_to_or2 = distance_between_two_points($or2_x, 0 , $or2_y, 0) / $speed;

        $order_ids = array($or1_number);
        $orders_destination_times = array($time_from_center_to_or1);
        $route = new Route($order_ids, $orders_destination_times);
        $all_routes->append($route);

        $order_ids = array($or2_number);
        $orders_destination_times = array($time_from_center_to_or2);
        $route = new Route($order_ids, $orders_destination_times);
        $all_routes->append($route);

    }
}
//остался один маршрут
else{
    $max_time_from_call_to_arrival = 60;
    $speed = 60;
    $or1_number = $sort_orders[$position_in_loop]->getNumber();
    $or1_x = $sort_orders[$position_in_loop]->getDestinationCoordsX();
    $or1_y = $sort_orders[$position_in_loop]->getDestinationCoordsY();

    $time_from_center_to_or1 = distance_between_two_points($or1_x, 0 , $or1_y, 0) / $speed;

    $order_ids = array($or1_number);
    $orders_destination_times = array($time_from_center_to_or1);
    $route = new Route($order_ids, $orders_destination_times);
    $all_routes->append($route);

}

//вывод заказов по мере их поступления к заказчику
foreach($all_routes as $route){
    if($route->getOrder2ID() != null){
        if($route->getOrder3ID() != null){
            print_r( 'номер заказа - '.$route->getOrder1ID(). ' '.
                'время доставки: '.$route->getOrder1DestinationTime()."\n".
                'номер заказа - '.$route->getOrder2ID(). ', '.
                'время доставки: '.$route->getOrder2DestinationTime()."\n".
                'номер заказа - '.$route->getOrder3ID(). ', '.
                'время доставки: '.$route->getOrder3DestinationTime().
                "\n");
        }
        else{
            print_r( 'номер заказа - '.$route->getOrder1ID(). ', '.
                'время доставки: '.$route->getOrder1DestinationTime()."\n".
                'номер заказа - '.$route->getOrder2ID(). ', '.
                'время доставки: '.$route->getOrder2DestinationTime().
                "\n");
        }
    }
    else{
        print_r( 'номер заказа - '.$route->getOrder1ID(). ', '.
            'время доставки: '.$route->getOrder1DestinationTime().
            "\n");
    }
}

